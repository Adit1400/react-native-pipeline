import React, { useEffect, useState } from 'react';
import { Button, SafeAreaView, Text } from 'react-native';
import { DefaultTheme, DarkTheme, useTheme } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

import { AppColors } from 'theme';

import styles from './app-styles';

const App = () => {
  const theme = useTheme();
  const [isDarkTheme, setIsDarkTheme] = useState(theme.dark);

  const CustomDarkTheme = {
    dark: true,
    colors: {
      ...DarkTheme.colors,
      primary: AppColors.primary,
    },
  };

  const CustomDefaultTheme = {
    dark: false,
    colors: {
      ...DefaultTheme.colors,
      primary: AppColors.primary,
    },
  };

  useEffect(() => SplashScreen.hide());

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          backgroundColor: isDarkTheme
            ? CustomDarkTheme.colors.background
            : CustomDefaultTheme.colors.background,
        },
      ]}
    >
      <Button
        title="Toggle theme"
        onPress={() => setIsDarkTheme(!isDarkTheme)}
      />
      <Text
        style={[
          styles.title,
          {
            color: isDarkTheme
              ? CustomDarkTheme.colors.primary
              : CustomDefaultTheme.colors.primary,
          },
        ]}
      >
        Title Text
      </Text>
      <Text
        style={[
          styles.subTitle,
          {
            color: isDarkTheme
              ? CustomDarkTheme.colors.text
              : CustomDefaultTheme.colors.text,
          },
        ]}
      >
        Sub Title Text
      </Text>
    </SafeAreaView>
  );
};

export default App;
