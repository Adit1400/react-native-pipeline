const aliases = {
  assets: './src/assets',
  common: './src/common',
  components: './src/components',
  config: './src/config',
  constants: './src/constants',
  firestore: './src/firestore',
  hooks: './src/hooks',
  locales: './src/locales',
  mst: './src/mst',
  navigation: './src/navigation',
  network: './src/network',
  screens: './src/screens',
  theme: './src/theme',
  utils: './src/utils',
  wrappers: './src/wrappers',
};

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ['module-resolver', { alias: aliases }],
    ['react-native-reanimated/plugin'],
  ],
};
