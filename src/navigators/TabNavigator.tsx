import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Tabs } from './Tabs';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator initialRouteName="HomeTab">
      {Tabs.map((item) => {
        const { component, id, name } = item;
        return (
          <Tab.Screen
            key={id}
            name={name}
            component={component}
            options={{
              tabBarLabel: name,
              headerShown: false,
              // tabBarIcon: ({ focused }) =>
              //   focused ? activeIcon : inActiveIcon,
            }}
          />
        );
      })}
    </Tab.Navigator>
  );
};

export default TabNavigator;
