import * as React from 'react';
import { useColorScheme } from 'react-native';
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';

// import TabNavigator from './TabNavigator';
// import DrawerNavigator from './DrawerNavigator';
import AppNavigator from './AppNavigator';

const RootNavigator = () => {
  let scheme = useColorScheme();
  return (
    <NavigationContainer theme={scheme === 'dark' ? DarkTheme : DefaultTheme}>
      {/* <DrawerNavigator /> */}
      {/* <TabNavigator /> */}
      <AppNavigator />
    </NavigationContainer>
  );
};

export default RootNavigator;
