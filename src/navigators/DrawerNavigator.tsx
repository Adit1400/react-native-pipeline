import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { DefaultTheme, useTheme } from '@react-navigation/native';

import TabNavigator from './TabNavigator';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  const theme = useTheme();

  return (
    <Drawer.Navigator
      screenOptions={{
        drawerType: 'front',
        drawerStyle: {
          backgroundColor: theme.dark
            ? '#1F1B24'
            : DefaultTheme.colors.background,
        },
      }}
      useLegacyImplementation
      // drawerContent={() => (
      //   <View style={{ marginTop: 80 }}>
      //     <Button
      //       title="Toggle Theme"
      //       onPress={() => setIsDarkTheme(!isDarkTheme)}
      //     />
      //   </View>
      // )}
    >
      <Drawer.Screen name="Main" component={TabNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
