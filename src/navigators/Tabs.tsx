import App from '../../App';
import More from '../screens/More/More';

export const Tabs = [
  {
    id: 'HomeTab',
    name: 'Home',
    component: App,
  },
  {
    id: 'MoreTab',
    name: 'More',
    component: More,
  },
];
