import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import DrawerNavigator from './DrawerNavigator';

const AppStack = createStackNavigator();

const AppNavigator = () => {
  return (
    <AppStack.Navigator>
      <AppStack.Screen
        name="Drawer"
        component={DrawerNavigator}
        options={{
          title: 'Back',
          headerShown: false,
        }}
      />
    </AppStack.Navigator>
  );
};

export default AppNavigator;
