import * as React from 'react';
import { SafeAreaView, Text } from 'react-native';
import { DefaultTheme, DarkTheme, useTheme } from '@react-navigation/native';

import styles from './more-styles';

const App = () => {
  const theme = useTheme();

  const CustomDarkTheme = {
    dark: true,
    colors: {
      ...DarkTheme.colors,
    },
  };

  const CustomDefaultTheme = {
    dark: false,
    colors: {
      ...DefaultTheme.colors,
    },
  };

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          backgroundColor: theme.dark
            ? CustomDarkTheme.colors.background
            : CustomDefaultTheme.colors.background,
        },
      ]}
    >
      <Text style={[styles.title]}>Title Text</Text>
      <Text style={[styles.subTitle]}>Sub Title Text</Text>
    </SafeAreaView>
  );
};

export default App;
