import { standardPalette } from './palettes/standardPalette';
import { BMGPalette } from './palettes/BMGPalette';
import { simsimPalette } from './palettes/simsimPalette';

const getPalette = (appName: string) => {
  switch (appName) {
    case 'BMG':
      return BMGPalette;
    case 'simsim':
      return simsimPalette;
    default:
      return standardPalette;
  }
};

const myPalette = getPalette('simsim');

export const AppColors = {
  myPalette,
  /**
   * The screen background.
   */
  backgroundColor: myPalette.backgroundColor,
  /**
   * The main tinting color.
   */
  primary: myPalette.primary,
  /**
   * The primary font family.
   */
  fontFamily: myPalette.fontFamily,
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: 'rgba(0, 0, 0, 0)',

  transparentBlack: 'rgba(0, 0, 0, 0.3)',
};
