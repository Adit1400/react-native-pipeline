import { RFValue } from 'react-native-responsive-fontsize';

/**
 * returns custom responsive font size according to device screen height
 * @param fontSize required font size
 */
const getFontSize = (fontSize: number): number => {
  return RFValue(fontSize, 844);
};

export { getFontSize };
