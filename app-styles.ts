import { StyleSheet } from 'react-native';
import { AppColors } from './src/theme';

const styles = StyleSheet.create({
  container: {
    backgroundColor: AppColors.backgroundColor,
    flex: 1,
  },
  title: {
    color: AppColors.primary,
  },
  subTitle: {
    color: AppColors.primary,
    fontSize: 20,
    fontFamily: AppColors.fontFamily,
  },
});

export default styles;
